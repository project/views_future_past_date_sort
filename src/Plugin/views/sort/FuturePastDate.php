<?php

namespace Drupal\views_future_past_date_sort\Plugin\views\sort;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\views\Plugin\views\sort\Date;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A sort handler for allowing selection of date fields to be ordered by date.
 *
 * Based off of code from:
 * https://chromatichq.com/blog/create-custom-views-sort-plugin-drupal-8
 *
 * @ViewsSort("future_past_date")
 */
class FuturePastDate extends Date {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Constructs a FuturePastDate object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountProxyInterface $account) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['future_past_date'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Future/Past Date Sort'),
      '#description' => $this->t('Sorts future dates before past dates, and secondarily sorts dates by distance from the current time'),
      '#default_value' => $this->options['future_past_date'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if (!$this->options['future_past_date']) {
      return parent::query();
    }
    // .
    $this->ensureMyTable();

    // Calculate the user's timezone offset. Default to UTC.
    if($this->account->getTimeZone()) {
      $timezone = new \DateTimeZone($this->account->getTimeZone());
    } else {
      $timezone = new \DateTimeZone('UTC');
    }

    $offset = $timezone->getOffset(new \DateTime());

    $date_alias = "UNIX_TIMESTAMP($this->tableAlias.$this->realField) - $offset";

    $this->query->addOrderBy(NULL,
      "UNIX_TIMESTAMP() > $date_alias",
      $this->options['order'],
      "in_past"
    );

    // How far in the past/future is this event?
    $this->query->addOrderBy(NULL,
      "ABS($date_alias - UNIX_TIMESTAMP() - $offset)",
      $this->options['order'],
      "distance_from_now"
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['future_past_date'] = ['default' => 0];

    return $options;
  }

}

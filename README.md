CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides a views field sort plugin to allow sorting future dates in 
chronological order before showing past events in reverse chronological order.

 * For a full description of the module visit:
   https://www.drupal.org/project/views_future_past_date_sort
  
 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/views_future_past_date_sort


REQUIREMENTS
------------

This module requires Drupal Core's Views module as well as Core's Datetime module. 


INSTALLATION
------------

Install the module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module. 
2. Navigate to a View. Add a Datetime field to a view.
3. Check the Future/Past Date Sort checkbox to allow sorting by Future/Past.

MAINTAINERS
-----------

 * Anthony Cooper - https://www.drupal.org/u/coops_
 